## Most wanted cookies

### Build & RUN with MAVEN
If you have java 17 installed you can run the following command to build
```
./mvnw package && java -jar target/most-wanted-cookies-finder.jar -f {file} -d {date}
```
OR
```
./run -f {cookiefile} -d {date}
```
---------------------------
### Build & RUN with docker

you don't need to have java 17 to run the application and by having docker installed you use the following commands to run it.

```
docker build -t most-wanted-cookies-finder .
docker run -v {absolute file name(full filename)}:/root/cookies.csv most-wanted-cookies-finder java -jar most-wanted-cookies-finder.jar --file cookies.csv --date {date}
```

