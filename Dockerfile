FROM openjdk:17 AS BUILD_IMAGE
ENV APP_HOME=/app
RUN mkdir -p $APP_HOME/src/
WORKDIR $APP_HOME
COPY pom.xml mvnw mvnw.cmd $APP_HOME/
COPY .mvn $APP_HOME/.mvn
COPY src/ $APP_HOME/src/
RUN ls $APP_HOME/src/
RUN ls $APP_HOME/
# download dependencies
RUN ./mvnw package

FROM openjdk:17
WORKDIR /root/
COPY --from=BUILD_IMAGE /app/target/most-wanted-cookies-finder.jar .
CMD ["java","-jar","most-wanted-cookies-finder.jar"]
