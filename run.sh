#!/bin/bash

JAVA_VERSION=$(java -version 2>&1)

if [[ ! $JAVA_VERSION =~ 'java version "17"' ]]
then
    echo "you must install java 17"
    exit 1
fi

## in order to be able to run this file; please install java 17 first
if [ ! -f "target/most-wanted-cookies-finder.jar" ]
then
    ./mvnw package
fi

java -jar target/most-wanted-cookies-finder.jar $1 $2 $3 $4
