package io.github.moghaffari.quantcast.cookie.processor;

public interface CookieStreamProcessor <R, P> {
    R process(P streamProvider);
}
