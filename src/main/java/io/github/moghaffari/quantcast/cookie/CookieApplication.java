package io.github.moghaffari.quantcast.cookie;

import io.github.moghaffari.quantcast.cookie.fs.LocalFileStreamProvider;
import io.github.moghaffari.quantcast.cookie.helper.CommandLineHelper;
import io.github.moghaffari.quantcast.cookie.helper.SystemHelper;
import io.github.moghaffari.quantcast.cookie.mapper.LineMapperImpl;
import io.github.moghaffari.quantcast.cookie.processor.MostActiveCookiesProcessor;
import lombok.extern.slf4j.Slf4j;

import static io.github.moghaffari.quantcast.cookie.helper.CommandLineHelper.*;

@Slf4j
public class CookieApplication {

    public static void main(String[] args) {
        CommandLineHelper commandLineHelper = new CommandLineHelper(new SystemHelper());
        var cli = commandLineHelper.parseCli(args);
        var date = cli.getOptionValue(DATE_ARG);
        var fileName = cli.getOptionValue(FILE_ARG);
        var separator = cli.getOptionValue(SEPARATOR_ARG, ",");
        try {
            log.info("looking for the most active cookies on date =  {}", date);
            var mostActiveCookies = new MostActiveCookiesProcessor(date, new LineMapperImpl(separator))
                    .process(new LocalFileStreamProvider(fileName));
            log.info("The most active cookies are:");
            mostActiveCookies.forEach(System.out::println);
        } catch (RuntimeException ex) {
            log.error(ex.getMessage());
        }
    }
}
