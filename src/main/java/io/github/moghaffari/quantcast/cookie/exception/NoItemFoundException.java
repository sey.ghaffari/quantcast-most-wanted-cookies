package io.github.moghaffari.quantcast.cookie.exception;

public class NoItemFoundException extends RuntimeException {
    public NoItemFoundException() {
        this("no item found.");
    }

    public NoItemFoundException(String message) {
        super(message);
    }

    public NoItemFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
