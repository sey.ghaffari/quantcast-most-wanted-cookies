package io.github.moghaffari.quantcast.cookie.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CookieUsageEntry {
    private final String cookie;
    private final String date;

    public static CookieUsageEntry from(@NonNull String[] columns) {
        if (columns.length < 2) {
            throw new IllegalArgumentException("At least two columns have to be presented.");
        }
        return new CookieUsageEntry(columns[0], columns[1]);
    }
}
