package io.github.moghaffari.quantcast.cookie.helper;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;

@Slf4j
public class CommandLineHelper {
    public static final String DATE_ARG = "d";
    public static final String FILE_ARG = "f";
    public static final String SEPARATOR_ARG = "s";

    private final SystemHelper systemHelper;

    public CommandLineHelper(SystemHelper systemHelper) {
        this.systemHelper = systemHelper;
    }

    public void printHelp(Options options) {
        var header = "Most active cookie finder\n\n";
        var footer = "\nPlease check at https://github.com/";

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("most-active-cookie-finder", header, options, footer, true);
    }

    public CommandLine parseCli(String[] args) {
        var parser = new DefaultParser();
        var options = createCliOptions();
        CommandLine cli = null;
        try {
            // parse the command line arguments
            cli = parser.parse(options, args);
        }
        catch (ParseException e) {
            log.error("Wrong arguments: {}", e.getMessage());
            printHelp(options);
            systemHelper.exit(1);
        }

        return cli;
    }

    private Options createCliOptions() {
        var options = new Options();
        options.addOption(Option.builder(FILE_ARG)
                .longOpt("file")
                .hasArg()
                .desc("The file name")
                .required()
                .build()
        );
        options.addOption(Option.builder(DATE_ARG)
                .longOpt("date")
                .hasArg()
                .desc("The date like 2018-12-09")
                .required()
                .build()
        );

        options.addOption(Option.builder(SEPARATOR_ARG)
                .longOpt("separator")
                .hasArg()
                .desc("The column separator, the default value is \",\" ")
                .build()
        );

        return options;
    }
}
