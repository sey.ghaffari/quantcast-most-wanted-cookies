package io.github.moghaffari.quantcast.cookie.fs;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

@Slf4j
public class LocalFileStreamProvider implements LineStreamProvider {

    private final String fileName;

    public LocalFileStreamProvider(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Stream<String> lines() {
        try {
            return Files.lines(Path.of(fileName));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
