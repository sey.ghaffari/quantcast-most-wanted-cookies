package io.github.moghaffari.quantcast.cookie.fs;

import java.util.stream.Stream;

public interface LineStreamProvider {
    Stream<String> lines();
}
