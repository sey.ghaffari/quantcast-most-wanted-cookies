package io.github.moghaffari.quantcast.cookie.mapper;

import io.github.moghaffari.quantcast.cookie.model.CookieUsageEntry;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;


/**
 * The {@code LineMapperImpl} class maps Line to  {@code CookieUsageEntry}.
 * <p> Passing a {@code null} argument to the map method
 * in this class will cause a {@link NullPointerException} to be
 * thrown.
 *
 * @author  Mohammad Ghaffari
 */

public class LineMapperImpl implements LineMapper<CookieUsageEntry> {

    private final String separator;

    public LineMapperImpl() {
        this(",");
    }

    public LineMapperImpl(String separator) {
        this.separator = separator;
    }

    @Override
    public CookieUsageEntry map(@NonNull String line) {
        return CookieUsageEntry.from(StringUtils.split(line, separator));
    }
}
