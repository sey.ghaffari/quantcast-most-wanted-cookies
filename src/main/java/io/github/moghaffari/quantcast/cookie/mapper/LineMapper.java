package io.github.moghaffari.quantcast.cookie.mapper;

public interface LineMapper<T> extends Mapper<String, T> {
    T map(String line);
}
