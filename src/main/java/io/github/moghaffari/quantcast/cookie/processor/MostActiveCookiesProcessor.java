package io.github.moghaffari.quantcast.cookie.processor;

import io.github.moghaffari.quantcast.cookie.exception.NoItemFoundException;
import io.github.moghaffari.quantcast.cookie.fs.LineStreamProvider;
import io.github.moghaffari.quantcast.cookie.mapper.LineMapper;
import io.github.moghaffari.quantcast.cookie.mapper.LineMapperImpl;
import io.github.moghaffari.quantcast.cookie.model.CookieUsageEntry;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
public class MostActiveCookiesProcessor implements CookieStreamProcessor<List<String>, LineStreamProvider> {

    private final String date;
    private final LineMapper<CookieUsageEntry> lineMapper;

    public MostActiveCookiesProcessor(String date) {
        this(date, new LineMapperImpl());
    }

    public MostActiveCookiesProcessor(String date, LineMapper<CookieUsageEntry> mapper) {
        this.date = date;
        this.lineMapper = mapper;
    }

    /**
     * Expects the {@code streamProvider} is sorted descending based on the date field.
     * @param streamProvider The stream provider that will be used to retrieve the lines
     * @return The list of most active cookies
     */
    @Override
    public List<String> process(@NonNull LineStreamProvider streamProvider) {
        log.info("looking for the most active cookies on date =  {}", date);
        Map<String, Long> accumulator = new ConcurrentHashMap<>();
        streamProvider.lines()
                .skip(1) // skipping the header
                .dropWhile(line -> StringUtils.isEmpty(line) || !line.contains(date)) // ignore the empty lines or the ones that don't contain the date
                .takeWhile(line -> StringUtils.isEmpty(line) || line.contains(date)) // accept till the lines contain the date or they're empty
                .parallel() // it's better to run the rest of the operation in a parallel stream
                .filter(Predicate.not(String::isEmpty))
                .map(lineMapper::map)
                .forEach(cu -> accumulator.merge(cu.getCookie(), 1L, Math::addExact));

        var max = accumulator.values()
                .stream()
                .max(Long::compareTo)
                .orElseThrow(NoItemFoundException::new);

        log.info("Max appearance is {}", max);

        return accumulator.entrySet()
                .parallelStream()
                .filter(it -> max.equals(it.getValue())).map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
