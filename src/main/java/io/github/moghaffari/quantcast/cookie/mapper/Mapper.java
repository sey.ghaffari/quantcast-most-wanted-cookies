package io.github.moghaffari.quantcast.cookie.mapper;

public interface Mapper<S, T> {
    T map(S s);
}
