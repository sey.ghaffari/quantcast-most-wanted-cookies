package io.github.moghaffari.quantcast.cookie.mapper;

import io.github.moghaffari.quantcast.cookie.model.CookieUsageEntry;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LineMapperImplTest {

    @Test
    void map() {
        var mapper = new LineMapperImpl();
        CookieUsageEntry entry = mapper.map("cookie,timestamp");
        assertEquals(entry.getCookie(), "cookie");
        assertEquals(entry.getDate(), "timestamp");
    }

    @Test
    void mapFailure() {
        var mapper = new LineMapperImpl();
        assertThrows(NullPointerException.class, () -> mapper.map(null));
    }
}
