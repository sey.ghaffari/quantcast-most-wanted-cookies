package io.github.moghaffari.quantcast.cookie.helper;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static io.github.moghaffari.quantcast.cookie.helper.CommandLineHelper.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class CommandLineHelperTest {

    @Test
    void parseCli() {
        var commandLineHelper = new CommandLineHelper(new SystemHelper());
        String[] args = {"-d", "2019-01-01", "-f", "test-case.csv", "-s", ";"};
        var options = commandLineHelper.parseCli(args);
        assertEquals(3, options.getOptions().length);
        assertEquals("2019-01-01", options.getOptionValue(DATE_ARG));
        assertEquals("test-case.csv", options.getOptionValue(FILE_ARG));
        assertEquals(";", options.getOptionValue(SEPARATOR_ARG));
    }

    @Test
    void parseCliFails() {
        SystemHelper systemHelper = Mockito.mock(SystemHelper.class);
        var commandLineHelper = Mockito.spy(new CommandLineHelper(systemHelper));
        String[] args = {"-d", "2019-01-01", "-s", ";"};
        var options = commandLineHelper.parseCli(args);
        verify(systemHelper, times(1)).exit(1);
        verify(commandLineHelper, times(1)).printHelp(any());
    }
}
