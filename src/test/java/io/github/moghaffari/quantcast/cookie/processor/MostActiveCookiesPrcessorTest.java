package io.github.moghaffari.quantcast.cookie.processor;

import io.github.moghaffari.quantcast.cookie.mapper.LineMapperImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static io.github.moghaffari.quantcast.cookie.TestUtil.loadTestCase;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MostActiveCookiesPrcessorTest {

    @Test
    void processWithDefaultSeparator() {
        var result = new MostActiveCookiesProcessor("2018-12-09").process(() -> loadTestCase("test-case1.csv"));
        assertEquals(1, result.size());
        assertEquals("AtY0laUfhglK3lC7", result.get(0));

        List<Object> expected = Arrays.asList("AtY0laUfhglK3lC7", "SAZuXPGUrfbcn5UA", "4sMM2LxV07bPJzwf", "fbcn5UAVanZf6UtG");
        result = new MostActiveCookiesProcessor("2018-12-08").process(() -> loadTestCase("test-case1.csv"));
        assertEquals(4, result.size());
        assertEquals(expected.stream().sorted().collect(Collectors.toList()), result.stream().sorted().collect(Collectors.toList()));
    }

    @Test
    void processWithSemicolon() {
        LineMapperImpl mapper = Mockito.spy(new LineMapperImpl(";"));
        var result = new MostActiveCookiesProcessor("2018-12-09", mapper).process(() -> loadTestCase("test-case2-semicolon.csv"));
        assertEquals(1, result.size());
        assertEquals("AtY0laUfhglK3lC7", result.get(0));

        Mockito.verify(mapper, Mockito.times(4)).map(Mockito.anyString());

        Mockito.reset(mapper);

        List<Object> expected = Arrays.asList("AtY0laUfhglK3lC7", "SAZuXPGUrfbcn5UA", "4sMM2LxV07bPJzwf", "fbcn5UAVanZf6UtA", "fbcn5UAVanZf6UtG");
        result = new MostActiveCookiesProcessor("2018-12-08", mapper).process(() -> loadTestCase("test-case2-semicolon.csv"));
        assertEquals(5, result.size());
        assertEquals(expected.stream().sorted().collect(Collectors.toList()), result.stream().sorted().collect(Collectors.toList()));
        Mockito.verify(mapper, Mockito.times(5)).map(Mockito.anyString());
    }


    // Test the date that doesn't exist in the file
    @Test
    void processFails() {
        assertThrows(RuntimeException.class,
                () -> new MostActiveCookiesProcessor("2018-12-06").process(() -> loadTestCase("test-case1.csv")),
                "no item found.");
    }
}
