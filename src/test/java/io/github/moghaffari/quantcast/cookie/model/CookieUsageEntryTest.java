package io.github.moghaffari.quantcast.cookie.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CookieUsageEntryTest {

    @Test
    void testFrom() {
        CookieUsageEntry entry = CookieUsageEntry.from(new String[]{"cookie", "date"});
        assertEquals("cookie", entry.getCookie());
        assertEquals("date", entry.getDate());

        entry = CookieUsageEntry.from(new String[]{"cookie1", "date1", "something else"});
        assertEquals("cookie1", entry.getCookie());
        assertEquals("date1", entry.getDate());
    }

    @Test
    void testFromWithException() {
        assertThrows(IllegalArgumentException.class, () -> CookieUsageEntry.from(new String[]{"cookie"}));
    }
}
