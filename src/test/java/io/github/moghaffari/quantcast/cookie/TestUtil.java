package io.github.moghaffari.quantcast.cookie;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Stream;

public class TestUtil {

    public static Stream<String> loadTestCase(String name) {
        return loadResource("test-cases/" + name);
    }

    @SneakyThrows
    public static Stream<String> loadResource(String name) {
        return new BufferedReader(new InputStreamReader(TestUtil.class.getClassLoader().getSystemResourceAsStream(name))).lines();
    }
}
